package ch.briggen.bfh.sparklist.domain;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;

import org.h2.jdbcx.JdbcDataSource;
import org.h2.tools.RunScript;

public class ItemDBTestHelper {
	static void initDataSourceForTest(String testName) throws Exception {
		JdbcDataSource ds = new JdbcDataSource();
		ds.setURL("jdbc:h2:mem:"+testName+";TRACE_LEVEL_SYSTEM_OUT=2;DB_CLOSE_DELAY=60");
		ds.setUser("sa");
		ds.setPassword("");
		
		JdbcRepositoryHelper.overrideDefalutDataSource(ds);
	}
	
	static void initDB() throws SQLException
	{
		try(Connection c = JdbcRepositoryHelper.getConnection())
		{
			ClassLoader myLoader = ItemRepositoryTest.class.getClassLoader();
			InputStream sqlscript = myLoader.getResourceAsStream("initdatabase.sql");
			RunScript.execute(c,new InputStreamReader(sqlscript));
		}
	}
	
}
